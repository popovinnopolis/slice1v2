package slice1v2;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import static slice1v2.Main.isStop;

public class Main {
    public static boolean isStop = false;

    public static void main(String[] args) {
        (new Thread(new Worker1())).start();
        (new Thread(new Worker2())).start();
    }
}

class Worker1 implements Runnable {
    private static Integer rndNum;

    private static synchronized void setRndNum(Integer rndNum) {
        Worker1.rndNum = rndNum;
    }

    public static synchronized Integer getRndNum() {
        return rndNum;
    }

    @Override
    public void run() {
        Thread th = Thread.currentThread();
        th.setName("Worker1");
        System.out.printf("%s started.\n", th.getName());

        Random r = new Random();
        while (!isStop) {
            Integer rndInt = r.nextInt(100);
            setRndNum(rndInt);
            System.out.printf("%s generate: %d.\n", th.getName(), rndInt);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}

class Worker2 implements Runnable {
    private static Map<Integer, Integer> res = new HashMap<>();
    private int counter = 0;

    @Override
    public void run() {
        Thread th = Thread.currentThread();
        th.setName("Worker2");
        System.out.printf("%s started.\n", th.getName());
        Integer curKey;
        while (!isStop) {
            curKey = Worker1.getRndNum();
            Integer curValue = res.containsKey(curKey) ? res.get(curKey) : 0;
            res.put(curKey, ++curValue);
            if (curValue >= 5) {
                isStop = true;
                System.out.printf("%s. Key: %d generated %d times.\nResult: %s\n",
                        th.getClass(), curKey, curValue, res);
            }
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            counter++;
            if (counter % 5 == 0) {
                System.out.printf("%s 5sec passed..\nResult: %s\n", th.getName(), res);
            }
        }
    }
}
